﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Target : MonoBehaviour {

	public Text render;
	
	// Update is called once per frame
	void Update () {
		this.render.text = GlobalTarget.currentTarget;
	}
}
