﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebSocket_Data {

    public float posLat;
    public float posLng;

	public WebSocket_Data() {
        
	}

    public WebSocket_Data(GpsPoint pos) {
        Debug.Log(pos);
        this.posLat = pos.lat;
        this.posLng = pos.lng;
    }

	public string toJSON()
	{
		return JsonUtility.ToJson(this);
	}

}

struct SocketPos {
    public float posLat;
    public float posLng;
}
