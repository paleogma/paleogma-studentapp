﻿using UnityEngine;
using System.Collections;
using System;

public class WebSocket_Client : MonoBehaviour {

    WebSocket_Event lastEvent;
    WebSocket_Data lastEventData;

	IEnumerator Start () {
		Utils.customLog (this.name, "Init.");
        DontDestroyOnLoad(this);

        if (Config.isTeacher) {
            GlobalSocketTeacher.StartSocketTeacher();
            yield return StartCoroutine(GlobalSocketTeacher.w.Connect());
            while (true)
            {
                string reply = GlobalSocketTeacher.w.RecvString();
                if (reply != null)
                {
                    Utils.customLog(this.name, "Received " + reply);

                    lastEvent = JsonUtility.FromJson<WebSocket_Event>(reply);
                    lastEventData = JsonUtility.FromJson<WebSocket_Data>(lastEvent.data);

                    switch (lastEvent.eventType)
                    {
                        case "startGame":
                            SceneChanger.Instance.fadeToMapScene();
                            break;
                        case "gpsPos":
                            var pos = new GpsPoint(lastEventData.posLat, lastEventData.posLng);
                            LocationTeacherGPS.movePin(pos);
                            break;
                        case "requestRapport":
                            Rapport.openPopUp();
                            break;
                        default:
                            break;
                    }

                }
                if (GlobalSocketTeacher.w.error != null)
                {
                    Utils.customLog(this.name, "Error " + reply);
                    break;
                }
                yield return 0;
            }
            GlobalSocketTeacher.w.Close();
        } else {
            GlobalSocketStudent.StartSocketStudent();
            yield return StartCoroutine(GlobalSocketStudent.w.Connect());
            while (true)
            {
                string reply = GlobalSocketStudent.w.RecvString();
                if (reply != null)
                {
                    Utils.customLog(this.name, "Received " + reply);

                    lastEvent = JsonUtility.FromJson<WebSocket_Event>(reply);
                    lastEventData = JsonUtility.FromJson<WebSocket_Data>(lastEvent.data);

                    switch (lastEvent.eventType)
                    {
                        case "startGame":
                            SceneChanger.Instance.fadeToMapScene();
                            break;
                        case "gpsPos":
                            var pos = new GpsPoint(lastEventData.posLat, lastEventData.posLng);
                            if (GlobalMapState.firstPath) {
                                LocationGPS.movePin(pos);
                                Map.Instance.rotatePin(new Vector3(0f, 0f, 0f));
                            } else {
                                LocationGPS.movePin(pos);
                                Map.Instance.rotatePin(new Vector3(180f, 0f, 0f));
                            }
                            break;
                        case "acceptRapport":
                            GlobalState.mammothEarned = true;
                            GlobalMapState.acceptedRapportPopUpActive = true;
                            break;
                        case "rejectRapport":
                            GlobalMapState.rejectedRapportPopUpActive = true;
                            break;
                        default:
                            break;
                    }

                }
                if (GlobalSocketStudent.w.error != null)
                {
                    Utils.customLog(this.name, "Error " + reply);
                    break;
                }
                yield return 0;
            }
            GlobalSocketStudent.w.Close();
        }

		
	}
}
