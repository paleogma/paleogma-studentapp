﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebSocket_Event {

	public string eventType;
	public string data;

	public WebSocket_Event(string eventType, WebSocket_Data data) {
		this.eventType = eventType;
		this.data = data.toJSON();
	}

	public string toJSON()
	{
		return JsonUtility.ToJson(this);
	}

}
