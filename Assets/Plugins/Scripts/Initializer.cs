﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Initializer : MonoBehaviour {

	public static Initializer Instance;

	void Awake()
	{
		if(Instance) {
			DestroyImmediate(gameObject);
		}
		else {
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
	}

	// Use this for initialization
	void Start () {
        GlobalState.StartGlobalState();
		GlobalCountDown.StartCountDown (TimeSpan.FromHours(1));
		GlobalTarget.StartTarget ();
		GlobalARSceneState.StartARSceneState ();
        GlobalTeacherMapState.StartTeacherMapState();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
