﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardLifePlaceSlider : MonoBehaviour {

	public Animator slider;

	public void toggle(int slide) {
		int current = this.slider.GetInteger ("currentSlide");
		if(current != slide) {
			this.slider.SetInteger ("currentSlide", slide);
		}
	}
}
