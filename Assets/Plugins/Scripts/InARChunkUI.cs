﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InARChunkUI : MonoBehaviour {

	public CanvasGroup cv;
	public GameObject leftMenu;
	public GameObject cardWrapper;

	// Use this for initialization
	void Update () {
		if(GlobalARSceneState.userIsInFossilChunk == true) {
			this.cv.alpha = 1;
		} else {
			this.cv.alpha = 0;
		}
		this.leftMenu.SetActive (GlobalARSceneState.userIsInFossilChunk);
		this.cardWrapper.SetActive (GlobalARSceneState.userIsInFossilChunk);
	}
}
