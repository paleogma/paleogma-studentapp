﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public sealed class RightMenu : MonoBehaviour {

	public Animator animator;

	static private bool opened = false;

	void Start() {
		this.animate ();
	}

	public void toggle() {
		RightMenu.opened = !RightMenu.opened;
		this.animate ();
	}

	private void animate() {
		this.animator.SetBool ("opened", RightMenu.opened);
	}

}
