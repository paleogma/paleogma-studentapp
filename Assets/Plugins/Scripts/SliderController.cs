﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SliderController : MonoBehaviour {

    Animator anim;
    int SlindingSecondSlideHash = Animator.StringToHash("SlidingSecondSlideIn");
    int SlindingThirdSlideHash = Animator.StringToHash("SlidingThirdSlideIn");

    public GameObject startBtn;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SlidingFirstSlide()
    {
        anim.SetBool(SlindingSecondSlideHash, true);
    }

    public void ChooseStudent()
    {
        startBtn.SetActive(Config.isTeacher);
        anim.SetBool(SlindingThirdSlideHash, true);
    }

    public void ChooseTeacher()
    {
        startBtn.SetActive(Config.isTeacher);
        anim.SetBool(SlindingThirdSlideHash, true);
    }
}
