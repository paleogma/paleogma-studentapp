﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Timers;

public class LocationGPS : MonoBehaviour
{

    private ExecutionPlan pathTimer;

    private GpsPoint startGpsPos = new GpsPoint(6.1057820320129395f, 45.90593338012695f);
    private GpsPoint endGpsPos = new GpsPoint (6.106057643890381f, 45.905494689941406f);

	private FakeGPS fakeGPS;

    private int cpt = 0;

	void Start ()
	{
        if (GlobalMapState.firstPath) {
            LocationGPS.movePin(startGpsPos);
            Map.Instance.rotatePin(new Vector3(0f, 0f, 0f));
        } else {
            LocationGPS.movePin(endGpsPos);
            Map.Instance.rotatePin(new Vector3(180f, 0f, 0f));
        }
		//this.fakeGPS = new FakeGPS();
        //		this.gpsPos = Map.Instance.getCenteredPoint();
        //this.gpsPos = Map.Instance.getPercentagePoint (0.33f, 0.54f);
        //this.gpsPos = new GpsPoint (6.1049f, 45.90637f);
        //this.gpsPos = new GpsPoint (6.10609f, 45.90584f);
        //this.initLocation (); 

        //for (int i = 0; i < this.fakeGPS.posCollection.Count; i++) {
        //    this.fakeGPS.next();
        //    this.gpsPos = this.fakeGPS.getCurrentPos();
        //    GlobalSocketStudent.emitStudentPos(this.gpsPos);
        //}
    }


	void Update ()
	{
        if (GlobalMapState.firstPath)
        {
            Map.Instance.rotatePin(new Vector3(0f, 0f, 0f));
        }
        else
        {
            Map.Instance.rotatePin(new Vector3(180f, 0f, 0f));
        }
		// Connection has failed
		//if (this.isLocationEnabled ()) {
			// fake data
//			this.gpsPos = new GpsPoint(Input.location.lastData.latitude, Input.location.lastData.longitude);

			//if (this.isInBounds ()) {
			//	// Do some stuff here
			//	this.handlePinIsInBounds ();
			//}
		//}
	}

	IEnumerator initLocation ()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start ();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1) {
			print ("Timed out");
			yield break;
		}
	}

	//	void updateTextLabel() {
	//		Text textLabel = GetComponentInChildren<Text>();
	//		string locationLabel = "Location: " + relativePosition.lat + " " + relativePosition.lng;
	//		textLabel.text = locationLabel;
	//	}

	bool isLocationEnabled ()
	{
		if (Input.location.status == LocationServiceStatus.Failed) {
			print ("Unable to determine device location");
			Input.location.Stop ();
			return false;
		}
		return true;
	}

	//void handlePinIsInBounds ()
	//{
	//	Vector2 relPos = Map.Instance.getRelPosFromGpsPos (this.gpsPos);
	//	relPos = Map.Instance.getInvertedXRelPos (relPos);
	//	relPos = Map.Instance.getInvertedYRelPos (relPos);

	//	Vector2 chunk = Map.Instance.getCurrentChunk (relPos);
	//	Map.Instance.updateUserPinPos (chunk);
	//}

	//bool isInBounds ()
	//{
	//	return (this.gpsPos.lat > Config.minLat)
	//	&& (this.gpsPos.lat < Config.maxLat)
	//	&& (this.gpsPos.lng > Config.minLng)
	//	&& (this.gpsPos.lng < Config.maxLng);
	//}

    public static void movePin(GpsPoint gpsPos)
    {
        Vector2 relPos = Map.Instance.getRelPosFromGpsPos(gpsPos);
        relPos = Map.Instance.getInvertedXRelPos(relPos);
        relPos = Map.Instance.getInvertedYRelPos(relPos);

        Vector2 chunk = Map.Instance.getCurrentChunk(relPos);
        Map.Instance.updateUserPinPos(chunk);

        if (chunk.x == 5 && chunk.y == 8 && GlobalMapState.firstPath) {
            GlobalMapState.startFossileProche = true;
        } else if (chunk.x == 6 && chunk.y == 5 && !GlobalMapState.firstPath) {
            GlobalMapState.requestRapport = true;
        }
        //Map.Instance.updateOriginalUserPinPos (relPos);
    }
}