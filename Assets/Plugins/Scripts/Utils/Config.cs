﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config {

    //Prod
    public const bool isProd = true;

    //Type
    public const bool isTeacher = true;

	// WebSocket
    public const string serverIP = isProd ? "paleogma.herokuapp.com" : "192.168.43.91";
    public const string serverPort = isProd ? "" : ":1337";
    public const string serverRoot = isTeacher ? "teachers" : "students";

	// GPS
	public const float minLat = 6.10454f;
	public const float maxLat = 6.10730f;
	public const float minLng = 45.90513f;
	public const float maxLng = 45.90659f;

	public const float difLat = maxLat - minLat;
	public const float difLng = maxLng - minLng;
		
	public static GpsPoint pointTopLeft = new GpsPoint(minLat, minLng);
	public static GpsPoint pointTopRight = new GpsPoint(minLat, maxLng);
	public static GpsPoint pointBottomLeft = new GpsPoint(maxLat, minLng);
	public static GpsPoint pointBottomRight = new GpsPoint(maxLat, maxLng);

	// iPad Resolution
	public const float resWidth = 2048f;
	public const float resHeight = 1536f;
}