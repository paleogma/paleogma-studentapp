using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public static class LocationTeacherGPS
{

    public static void movePin (GpsPoint gpsPos)
    {
        Vector2 relPos = Map.Instance.getRelPosFromGpsPos (gpsPos);
        relPos = Map.Instance.getInvertedXRelPos (relPos);
        relPos = Map.Instance.getInvertedYRelPos (relPos);

        Vector2 chunk = Map.Instance.getCurrentChunk (relPos);
        Map.Instance.updateUserPinPos (chunk);
        //Map.Instance.updateOriginalUserPinPos (relPos);
    }
}