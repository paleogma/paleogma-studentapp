﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeGPS
{
	public List<GpsPoint> posCollection = new List<GpsPoint> ();

	private int current = 0;

	public FakeGPS ()
	{
		this.posCollection.Add (Map.Instance.getPercentagePoint (0.45f, 0.55f));
        this.posCollection.Add (Map.Instance.getPercentagePoint (0.55f, 0.55f));
        this.posCollection.Add (Map.Instance.getPercentagePoint (0.55f, 0.45f));
        this.posCollection.Add (Map.Instance.getPercentagePoint (0.55f, 0.35f));
        this.posCollection.Add (Map.Instance.getPercentagePoint (0.55f, 0.25f));
	}

//	public void OnApplicationQuit() {
//		this.pathTimer.Abort ();
//	}

	public void next ()
	{
		this.current += 1;
		if (this.current == this.posCollection.Count) {
			this.current = 0;
		}
	}

	public GpsPoint getCurrentPos ()
	{
		return this.posCollection [this.current];
	}

	public void prev ()
	{
		this.current -= 1;
		if (this.current < 0) {
			this.current = this.posCollection.Count - 1;
		}
	}
}

