﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils {

	public static void customLog(string className, object info) {
		string wantedLog = "<color=brown>[" + className +  "]</color> : " + info;
		Debug.Log(wantedLog);
	}

	public static void timeLog(object std) {
		Debug.Log("<color=blue>[" + DateTime.Now.ToString("h:mm:ss tt") +  "]</color> : " + std);
	}

//	public static GpsPoint getCurrentRelativeLocation(float currentLat, float currentLng) {
//		float tempLat = ifNegativeToZero(currentLat - Config.minLat) / Config.ratioLat * Config.resolutionHeight;
//		float tempLng = ifNegativeToZero(currentLng - Config.minLng) / Config.ratioLng * Config.resolutionWidth;
//
//		return new GpsPoint (tempLat, tempLng);
//	}

	public static float ifNegativeToZero(float number) {
		return (number < 0 ? 0 : number);
	}

	/* return %tage = of value in the range [ min - max ] / 100 */
	public static float norm(float value, float min, float max) {
		return (value - min) / (max - min);
	}

	/* return value = of percentage in the range [ min - max ] */
	public static float lerp(float norma, float min, float max) {
		return (max - min) * norma + min;
	}

	public static float map(float value, float sourceMin, float sourceMax, float destMin, float destMax) {
		return Utils.lerp(Utils.norm(value, sourceMin, sourceMax), destMin, destMax);
	}


}
