﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

	public Animator animator;

	private int sceneToLoad;

	#region SINGLETON PATTERN
	private static SceneChanger instance;

	private SceneChanger(){}
	static SceneChanger(){}

	public static SceneChanger Instance {
		get { return instance; }
	}
	#endregion

	void Start () {
		if (!instance) {
			instance = this;
		}
	}

	public void fadeToScene(int sceneIndex) {
		this.sceneToLoad = sceneIndex;
		this.animator.SetTrigger ("FadeOut");
	}

	public void onFadeComplete() {
		SceneManager.LoadScene (this.sceneToLoad);
	}

	public void fadeToARScene() {
		this.fadeToScene (3);
	}

	public void fadeToMapScene() {
		this.fadeToScene (2);
	}

    public void fadeToTeacherMapScene()
    {
        this.fadeToScene(1);
    }
}
