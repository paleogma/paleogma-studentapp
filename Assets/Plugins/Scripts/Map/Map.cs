﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public sealed class Map : MonoBehaviour {
	
	public RawImage userPin;
	public RawImage originalUserPin;
	public GameObject pins;
	private Vector2 nbChunk = new Vector2 (10, 10);

	#region SINGLETON PATTERN
	private static Map instance;

	private Map(){}
	static Map(){}

	public static Map Instance {
		get { return instance; }
	}
	#endregion

	void Start () {
		if (!instance) {
			instance = this;
		}

//		gpsPoint test = Utils.getCurrentRelativeLocation(45.90793f, 6.10295f);
//		updateUserPin (test.lat, test.lng);
	}

	public void updateUserPinPos(Vector2 chunk) {
		Vector2 pos = new Vector2 (0, 0);
		Vector2 chunkSize = this.getChunkSize ();
		pos.x = chunkSize.x * (chunk.x - 1) + chunkSize.x / 2;
		pos.y = chunkSize.y * (chunk.y - 1) + chunkSize.y / 2;
		this.updatePinPos(this.userPin, pos);
	}

	public void updateOriginalUserPinPos(Vector2 pos) {
		this.updatePinPos(this.originalUserPin, pos);
	}

	public void updatePinPos(RawImage pin, Vector2 pos) {
		pin.rectTransform.localPosition = pos;
	}

	public Vector2 getRelPosFromGpsPos(GpsPoint gps) {
		float relLat = Utils.map (gps.lat, Config.minLat, Config.maxLat, 0, Config.resWidth);
		float relLng = Utils.map (gps.lng, Config.minLng, Config.maxLng, 0, Config.resHeight);
		return new Vector2 (relLat, relLng);
	}

	public GpsPoint getCenteredPoint() {
		return new GpsPoint (Config.minLat + Config.difLat / 2, Config.minLng + Config.difLng / 2);
	}

	public GpsPoint getPercentagePoint(float xTage, float yTage) {
		return new GpsPoint (Config.minLat + Config.difLat * xTage, Config.minLng + Config.difLng * yTage);
	}

	public Vector2 getInvertedXRelPos(Vector2 pos) {
		return new Vector2 (Config.resWidth - pos.x, pos.y);
	}

	public Vector2 getInvertedYRelPos(Vector2 pos) {
		return new Vector2 (pos.x, Config.resHeight - pos.y);
	}

	public Vector2 getCurrentChunk(Vector2 pos) {
		Vector2 chunk = new Vector2(0, 0);
		chunk.x = (float) Math.Floor(pos.x / Config.resWidth * this.nbChunk.x) + 1;
		chunk.y = (float) Math.Floor(pos.y / Config.resHeight * this.nbChunk.y) + 1;

		return chunk;
	}

	public Vector2 getChunkSize() {
		Vector2 chunkSize = new Vector2(0, 0);
		chunkSize.x = Config.resWidth / this.nbChunk.x;
		chunkSize.y = Config.resHeight / this.nbChunk.y;
		return chunkSize;
	}
	 
    public void rotatePin(Vector3 rotation) {
        userPin.transform.localRotation = Quaternion.Euler(rotation);
    }
}
