﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pin {

	public RawImage image;
	public GpsPoint position {
		get { return position; }
		set { position = value; }
	}
	
	public Pin(string textureName, GpsPoint position) {
//		this.image = new RawImage ();
		this.image.texture = (Texture)Resources.Load (textureName);
		this.position = position;
	}
}
