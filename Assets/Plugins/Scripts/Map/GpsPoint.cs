﻿public struct GpsPoint {
	public float lat, lng;

	public  GpsPoint(float lat, float lng) {
		this.lat = lat;
		this.lng = lng;
	}
}