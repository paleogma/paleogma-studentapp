﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RapportStudentRejected : MonoBehaviour
{

    public GameObject self;

    public static void closePopUp() {
        GlobalMapState.rejectedRapportPopUpActive = false;
        GlobalTarget.setStep5();
        GlobalState.reset();
        GlobalSocketStudent.emitLeaveBase();
    }

    public void handleClose() {
        RapportStudentRejected.closePopUp();
    }

    void Update()
    {
        if (GlobalMapState.rejectedRapportPopUpActive) {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        } else {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
