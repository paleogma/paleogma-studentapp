﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rapport : MonoBehaviour {

    public GameObject self;

    public void handleAccept() {
        Rapport.closePopUp();
        GlobalSocketTeacher.emitAcceptRapport();
    }

    public void handleReject() {
        Rapport.closePopUp();
        GlobalSocketTeacher.emitRejectRapport();
    }

    public static void closePopUp() {
        GlobalTeacherMapState.questionPopUpActive = false;
    }

    public static void openPopUp() {
        GlobalTeacherMapState.questionPopUpActive = true;
    }

    void Update()
    {
        if (GlobalTeacherMapState.questionPopUpActive)
        {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
