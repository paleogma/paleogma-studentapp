﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lancement : MonoBehaviour
{

    public GameObject self;

    public static void closePopUp() {
        GlobalMapState.startExpeditionPopUpActive = false;
    }

    public void handleClose() {
        Lancement.closePopUp();
        GlobalSocketStudent.emitLeaveBase();
    }

    void Update()
    {
        if (GlobalMapState.startExpeditionPopUpActive)
        {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
