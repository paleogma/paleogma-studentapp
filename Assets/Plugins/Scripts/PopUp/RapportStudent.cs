﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RapportStudent : MonoBehaviour {

    public GameObject self;

    public void closePopUp() {
        GlobalMapState.requestRapport = false;
    }

    public void handleAccept() {
        this.closePopUp();
        GlobalSocketStudent.emitRequestRapport();
    }

    void Update()
    {
        if (GlobalMapState.requestRapport)
        {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
