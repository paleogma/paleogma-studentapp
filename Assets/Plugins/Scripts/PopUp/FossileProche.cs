﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FossileProche : MonoBehaviour
{

    public GameObject self;

    public static void closePopUp() {
        GlobalMapState.startFossileProche = false;
    }

    public void handleClose() {
        FossileProche.closePopUp();
    }

    void Update()
    {
        if (GlobalMapState.startFossileProche) {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        } else {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
