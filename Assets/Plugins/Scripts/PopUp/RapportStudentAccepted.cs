﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RapportStudentAccepted : MonoBehaviour
{

    public GameObject self;

    public static void closePopUp() {
        GlobalMapState.acceptedRapportPopUpActive = false;
        GlobalTarget.setStep1();
    }

    public void handleClose() {
        RapportStudentAccepted.closePopUp();
    }

    void Update()
    {
        if (GlobalMapState.acceptedRapportPopUpActive) {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        } else {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
