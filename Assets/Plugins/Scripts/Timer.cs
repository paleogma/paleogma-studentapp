﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {

	public Text render;
	public Image radial;
	bool isOver = false;

	void Update () {

		this.render.text = GlobalCountDown.TimeLeft.ToString();

		this.radial.fillAmount = (float) (GlobalCountDown.TimeLeft.TotalSeconds / GlobalCountDown.TotalTime.TotalSeconds);

		if (GlobalCountDown.TimeLeft.TotalSeconds < 0) {
			if (!this.isOver) {
				this.isOver = true;
				this.handleIsOver ();
			}

		}
	}

	void handleIsOver() {
		Debug.Log ("game over");
	}
}
