﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftMenu : MonoBehaviour {

	public Animator slider;

	public void toggle(int slide) {
		int current = this.slider.GetInteger ("activeCard");
		if(current == slide) {
			this.slider.SetInteger ("activeCard", 0);
		} else {
			this.slider.SetInteger ("activeCard", slide);
		}
	}
}
