﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalCountDown
{
	static DateTime TimeStarted;
	public static TimeSpan TotalTime;
	static bool Started;

	public static void StartCountDown(TimeSpan totalTime)
	{
		if(!Started) {
			Started = true;
			TimeStarted = DateTime.UtcNow;
			TotalTime = totalTime;
		}
	}

	public static TimeSpan TimeLeft
	{
		get
		{
			var result = TotalTime - (DateTime.UtcNow - TimeStarted);
			if (result.TotalSeconds <= 0) {
				result = TimeSpan.Zero;
			}
			
			return new TimeSpan (result.Hours, result.Minutes, result.Seconds);
		}
	}
}
