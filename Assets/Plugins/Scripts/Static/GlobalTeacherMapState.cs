using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalTeacherMapState
{
    static bool Started;

    public static void StartTeacherMapState() {
        if(!Started) {
            Started = true;
        }
    }

    public static bool questionPopUpActive = false;
}
