﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalTarget
{
	public static String currentTarget;
	static bool Started;

	public static void StartTarget() {
		if(!Started) {
			Started = true;
			GlobalTarget.setStep1 ();
		}
	}

	public static void setStep1() {
		currentTarget = "Débusquez les fossiles dispersés sur le site";
	}

	public static void setStep2() {
		currentTarget = "Survolez le fossile avec la tablette et regardez autour de vous";
	}

	public static void setStep3() {
		currentTarget = "Trouvez les indices autour de l'animal pour récolter des informations";
	}

	public static void setStep4() {
		currentTarget = "Retournez à la base pour rendre votre rapport";
	}

	public static void setStep5() {
		currentTarget = "Retournez sur le terrain de fouille pour approfondir vos recherches";
	}
}
