using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalMapState
{
    static bool Started;

    public static void StartGlobalMapState() {
        if(!Started) {
            Started = true;
        }
    }

    public static bool startExpeditionPopUpActive = true;
    public static bool startFossileProche = false;
    public static bool firstPath = true;
    public static bool requestRapport = false;
    public static bool acceptedRapportPopUpActive = false;
    public static bool rejectedRapportPopUpActive = false;
    public static bool inventoryIsActive = false;
}
