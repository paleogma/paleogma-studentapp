﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalARSceneState
{
	static bool Started;

	public static void StartARSceneState() {
		if(!Started) {
			Started = true;
		}
	}

	public static bool userIsInFossilChunk = false;
}
