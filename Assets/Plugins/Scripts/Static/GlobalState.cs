using UnityEngine;
using System.Collections;

public class GlobalState : MonoBehaviour
{

    static bool Started;

    public static void StartGlobalState()
    {
        if (!Started)
        {
            Started = true;
        }
    }

    public static void reset()
    {
        GlobalARSceneState.userIsInFossilChunk = false;

        GlobalMapState.startFossileProche = false;
        GlobalMapState.firstPath = true;
        GlobalMapState.requestRapport = false;
        GlobalMapState.acceptedRapportPopUpActive = false;
        GlobalMapState.rejectedRapportPopUpActive = false;

        GlobalState.isInventoryOpen = false;
    }

    public static bool mammothEarned = false;

    public static bool isInventoryOpen = false;
}

//public static bool startExpeditionPopUpActive = true;
//public static bool startFossileProche = false;
//public static bool firstPath = true;
//public static bool requestRapport = false;
//public static bool acceptedRapportPopUpActive = false;
//public static bool rejectedRapportPopUpActive = false;