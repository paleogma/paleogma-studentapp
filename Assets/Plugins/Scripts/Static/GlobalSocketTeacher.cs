﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalSocketTeacher
{
	static bool Started;
    public static WebSocket w;

    public static void StartSocketTeacher() {
		if(!Started) {
			Started = true;
            w = new WebSocket(new Uri("ws://" + Config.serverIP + Config.serverPort + "/" + Config.serverRoot));
		}
	}

    public static void emitStartGame() {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("startGame", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketTeacher", "Send " + json);

        w.SendString(json);
    }

    public static void emitAcceptRapport()
    {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("acceptRapport", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketTeacher", "Send " + json);

        w.SendString(json);
    }


    public static void emitRejectRapport()
    {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("rejectRapport", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketTeacher", "Send " + json);

        w.SendString(json);
    }
}
