﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalSocketStudent
{
	static bool Started;
    public static WebSocket w;

	public static void StartSocketStudent() {
		if(!Started) {
			Started = true;
            w = new WebSocket(new Uri("ws://" + Config.serverIP + Config.serverPort + "/" + Config.serverRoot));
		}
	}

    public static void emitStudentPos(GpsPoint gpsPoint)
    {
        var data = new WebSocket_Data(gpsPoint);
        var obj = new WebSocket_Event("gpsPos", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketStudent", "Send " + json);

        w.SendString(json);
    }

    public static void emitLeaveBase()
    {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("leaveBase", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketStudent", "Send " + json);

        w.SendString(json);
    }


    public static void emitReturnToBase()
    {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("returnToBase", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketStudent", "Send " + json);

        w.SendString(json);
    }

    public static void emitRequestRapport()
    {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("requestRapport", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketStudent", "Send " + json);

        w.SendString(json);
    }

    public static void emitReset()
    {
        var data = new WebSocket_Data();
        var obj = new WebSocket_Event("reset", data);
        string json = obj.toJSON();

        Utils.customLog("GlobalSocketStudent", "Send " + json);

        w.SendString(json);
    }
}
