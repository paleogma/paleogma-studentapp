﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;


public class GenerateImageAnchor : MonoBehaviour {

	[SerializeField]
	private ARReferenceImage referenceImage;

	[SerializeField]
	private GameObject prefabToGenerate;

	private GameObject imageAnchorGO;

    //public AudioClip mammouthAwake;
    //AudioSource audioSource;


	// Use this for initialization
	void Start () {
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
        //audioSource = GetComponent<AudioSource>();
	}

	void AddImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.Log ("image anchor added");
		if (arImageAnchor.referenceImageName == referenceImage.imageName) {

            GlobalTarget.setStep3();
            GlobalARSceneState.userIsInFossilChunk = true;

            Matrix4x4 wantedPosition = prefabToGenerate.transform.localToWorldMatrix * arImageAnchor.transform;
            //Matrix4x4 wantedPosition = arImageAnchor.transform;

            Vector3 position = UnityARMatrixOps.GetPosition (wantedPosition);
            //Quaternion rotation = UnityARMatrixOps.GetRotation(wantedPosition);
            Quaternion rotation = Quaternion.Euler(0,0,0);

			imageAnchorGO = Instantiate<GameObject> (prefabToGenerate, position, rotation);

            //audioSource.PlayOneShot(mammouthAwake, 0.7F);
		}
	}

	void UpdateImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.Log ("image anchor updated");
		if (arImageAnchor.referenceImageName == referenceImage.imageName) {
            Matrix4x4 wantedPosition = prefabToGenerate.transform.localToWorldMatrix * arImageAnchor.transform;

            imageAnchorGO.transform.position = UnityARMatrixOps.GetPosition (wantedPosition);
            //imageAnchorGO.transform.rotation = UnityARMatrixOps.GetRotation (wantedPosition);
		}

	}

	void RemoveImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.Log ("image anchor removed");
		if (imageAnchorGO) {
			GameObject.Destroy (imageAnchorGO);
		}

	}

	void OnDestroy()
	{
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;

	}

	// Update is called once per frame
	void Update () {
		
	}
}
