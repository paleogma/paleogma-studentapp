﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class handleMammothInteractions : MonoBehaviour {

	Animator anim;
	int cuddleHash = Animator.StringToHash("triggerCuddle");
	int footStompHash = Animator.StringToHash("triggerFootStomp");
    //public AudioClip mammouthRoar;
    //AudioSource audioSource;

    // Use this for initialization
    void Start () {
		anim = GetComponent<Animator>();
        //audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown() {
		bool choice = Mathf.Round(Random.value) == 0.0;

		anim.SetTrigger (choice == true ? cuddleHash : footStompHash);
        //audioSource.PlayOneShot(mammouthRoar, 0.7F);
    }
}
