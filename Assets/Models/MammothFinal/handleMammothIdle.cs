﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handleMammothIdle : StateMachineBehaviour {

    private Transform[] targets = new Transform[4];
    private string wantedTag = "Sphere";
    private Vector3 wantedSize = new Vector3(1,1,1);
    private float speed = 7;
    
	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Transform transform = animator.transform;
        int index = 0;
        foreach (Transform curTransform in transform)
        {
            if (curTransform.tag == wantedTag)
            {
                targets[index] = curTransform;
                index += 1;
            }
        }

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        foreach (Transform currentTarget in targets)
        {
            if (currentTarget.tag == wantedTag)
            {
                currentTarget.localScale = Vector3.Lerp(currentTarget.localScale, wantedSize, speed * Time.deltaTime);
            }
        }
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
