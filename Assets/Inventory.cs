﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public GameObject self;
    public GameObject empty;
    public GameObject full;

    public static void closePopUp()
    {
        GlobalMapState.inventoryIsActive = false;
    }

    public void handleClose()
    {
        Inventory.closePopUp();
    }

    public void toggle() {
        GlobalState.isInventoryOpen = !GlobalState.isInventoryOpen;
    }

    void Update()
    {
        if (GlobalState.mammothEarned)
        {
            full.transform.localScale = new Vector3(1f, 1f, 1f);
            empty.transform.localScale = new Vector3(0f, 0f, 0f);
        }
        else
        {
            full.transform.localScale = new Vector3(0f, 0f, 0f);
            empty.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        if (GlobalState.isInventoryOpen)
        {
            self.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            self.transform.localScale = new Vector3(0f, 0f, 0f);
        }
    }
}
